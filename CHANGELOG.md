# Change Log

## [0.8.0]

### Changed

- Loosen gunicorn dependency constraint to allow gunicorn >=22.0.0

## [0.7.2]

### Added

- Release docker images through multi-stage-docker-build CI template

## [0.7.1]

### No Change

- Release is just to test pipelines post repo move

## [0.7.0]

### Added

- Logs now handled by ucam-observe, including unified log level setting
- Need for gunicorn.conf.py removed

## [0.6.0]

### Added

- Set CPU and memory limits

### Fixed

- Remove external access to endpoint (was already authenticated)

## [0.5.0]

### Added

- Persistence for dead letter queue (short lived)
- Additional alert for expiring dead letter queue events

### Changed

- Function related variables moved to map/object

## [0.4.0]

### Added

- First built-in alert on events/messages failing after retries
- basic Cloud Event test factory

### Fixed

- `cloud_event` based functions exception handling fixed

## [0.3.0]

### Added

- Crontab style schedule option added to terraform module

## [0.2.0]

### Added

- Additional example functions
- Basic exception handling for functions

### Changed

- Removed/renamed example functions
- Change test tooling function naming, `event_app_client` => `event_app_test_client_factory`
