class UCAMFAASException(Exception):
    pass


class UCAMFAASCouldNotProcess(UCAMFAASException):
    pass
