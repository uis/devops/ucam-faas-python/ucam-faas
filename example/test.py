from unittest.mock import patch

from ucam_faas.testing import CloudEventFactory

from .main import example_cloud_event, example_raw_event

# Register the ucam_faas testing module as a pytest plugin - as it provides fixtures that we can
# make use of when testing the functions.
pytest_plugins = ["ucam_faas.testing"]


def test_example_raw_event():
    """
    Test the example_raw_event function using the undecorated version provided through the
    decorator library interface.

    This effectively tests the function 'raw', i.e. without any of the surrounding functions
    framework code in place. This example should be the primary way most tests are run for event
    functions, only in cases where the HTTP interaction is necessary should the app client be
    used.
    """

    with patch("example.main.patch_me") as patched_patch_me:
        assert example_raw_event.__wrapped__({}) is None

        patched_patch_me.assert_called_once()


def test_client_example_raw_event(event_app_test_client_factory):
    """
    Test the example_raw_event function via a test HTTP client.

    This tests the function is responding in a HTTP environment. This test support is provided
    for verifying the function is registering as expected. Tests that can be written using the
    '__wrapped__' interface should be preferred.
    """
    eac = event_app_test_client_factory(target="example_raw_event", source="example/main.py")
    response = eac.get("/")
    assert response.status_code == 200


def test_example_cloud_event():
    with patch("example.main.patch_me") as patched_patch_me:
        example_cloud_event.__wrapped__({"event": "yes!"})

        patched_patch_me.assert_called_once_with({"event": "yes!"})


def test_client_example_cloud_event(event_app_test_client_factory):
    eac = event_app_test_client_factory(target="example_cloud_event", source="example/main.py")

    # NOTE: When patching using event_app_test_client_factory, the module path will be relative
    # to the source file
    with patch("main.patch_me") as patched_patch_me:
        eac.post("/", json=CloudEventFactory.build(data={"event": "yes!"}).model_dump())

        patched_patch_me.assert_called_once()
        assert patched_patch_me.call_args.args[0] == {"event": "yes!"}


def test_status_and_healthy(event_app_test_client_factory):
    eac = event_app_test_client_factory(target="example_raw_event", source="example/main.py")
    response = eac.get("/healthy")
    assert response.status_code == 200
    assert response.text == "ok"

    response = eac.get("/status")
    assert response.status_code == 200
    assert response.text == "ok"
