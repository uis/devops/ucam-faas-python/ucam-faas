import random

from ucam_observe import get_structlog_logger

import ucam_faas

logger = get_structlog_logger(__name__)


# This function is included to aid test observations
def patch_me(data):
    pass


@ucam_faas.raw_event
def example_raw_event(raw_event):
    logger.info("example_raw_event", raw_event=raw_event)

    # Some logs to help manually test integration with ucam-observe
    logger.debug("example_raw_event_debug", raw_event=raw_event)
    logger.warn("example_raw_event_warn", raw_event=raw_event)

    patch_me(raw_event)


@ucam_faas.raw_event
def example_raw_event_fail(raw_event):
    logger.info("got_raw_event", raw_event=raw_event)

    patch_me(raw_event)

    raise ucam_faas.exceptions.UCAMFAASCouldNotProcess


@ucam_faas.cloud_event
def example_cloud_event(cloud_event):
    logger.info("got_cloud_event", cloud_event=cloud_event)

    patch_me(cloud_event)


@ucam_faas.cloud_event
def example_cloud_event_fail(cloud_event):
    logger.info("got_cloud_event_fail", cloud_event=cloud_event)

    patch_me(cloud_event)

    raise ucam_faas.exceptions.UCAMFAASCouldNotProcess


@ucam_faas.cloud_event
def example_cloud_event_sometimes_fail(cloud_event):
    logger.info("got_cloud_event_fail", cloud_event=cloud_event)

    patch_me(cloud_event)

    if bool(random.getrandbits(1)):
        raise ucam_faas.exceptions.UCAMFAASCouldNotProcess
