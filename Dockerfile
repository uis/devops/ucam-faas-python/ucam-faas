#

###############################################################################
# Base image for all others to build upon.
#
# If you change the version here, update pyproject.toml and the version used in
# CI.
FROM registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/python:3.11-slim AS base

# Some performance and disk-usage optimisations for Python within a docker container.
ENV PYTHONUNBUFFERED=1 PYTHONDONTWRITEBYTECODE=1

WORKDIR /usr/lib

RUN adduser --system --no-create-home nonroot

# Pretty much everything from here on needs poetry.
RUN pip install --no-cache-dir poetry

COPY pyproject.toml poetry.lock README.md ./
COPY ucam_faas ./ucam_faas

RUN poetry config virtualenvs.create false && \
    poetry install --without=dev

WORKDIR /usr/src/app

USER nonroot

HEALTHCHECK --interval=5m --timeout=3s \
  CMD curl -f http://localhost/healthy || exit 1

ENTRYPOINT ["ucam-faas", "--target"]

###############################################################################
# Example Dockerfile image
FROM base AS base-example

COPY example ./example
WORKDIR /usr/src/app/example

# Example testing image
FROM base-example AS tox

RUN pip install tox
ENTRYPOINT ["tox"]
CMD []

# Example full image
FROM base-example AS example

# If you need install/add anything with root permissions you'll need to switch to root
USER root

# If our function had requirements we could install them now:
# RUN pip install -r requirements.txt
# or, since poetry is available:
# RUN poetry install --no-root

# Make sure to swicth back to a non root user
USER nonroot

# Set the CMD to the default function to call
CMD ["example_raw_event"]

###############################################################################
# Base image for all others to build upon.
#
# We specify it as last stage so that a docker build without target will use this

FROM base AS full
